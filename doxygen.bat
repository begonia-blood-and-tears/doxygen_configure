@echo off
setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
set PATH=D:\msys64\compilers\graphviz\bin;!PATH!
set DOXYGEN=D:\msys64\compilers\doxygen\doxygen.exe
rem !DOXYGEN! -g pbc_doxygen_configure
!DOXYGEN! pbc_doxygen_configure_svg
!DOXYGEN! pbc_doxygen_configure_png
endlocal
@echo on
